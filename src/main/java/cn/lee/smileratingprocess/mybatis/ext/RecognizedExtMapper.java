package cn.lee.smileratingprocess.mybatis.ext;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RecognizedExtMapper {

    List<Long> findRecognizedIds();

    List<String> findUnRecognizedIds();

}
