package cn.lee.smileratingprocess.mybatis.mapper;

import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxx;
import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxxExample.Criteria;
import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxxExample.Criterion;
import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxxExample;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.jdbc.SQL;

public class TZpGrwxzxxSqlProvider {

    public String countByExample(TZpGrwxzxxExample example) {
        SQL sql = new SQL();
        sql.SELECT("count(*)").FROM("T_ZP_GRWXZXX");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    public String deleteByExample(TZpGrwxzxxExample example) {
        SQL sql = new SQL();
        sql.DELETE_FROM("T_ZP_GRWXZXX");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    public String insertSelective(TZpGrwxzxx record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("T_ZP_GRWXZXX");
        
        if (record.getXgh() != null) {
            sql.VALUES("xgh", "#{xgh,jdbcType=VARCHAR}");
        }
        
        if (record.getWxz() != null) {
            sql.VALUES("wxz", "#{wxz,jdbcType=VARCHAR}");
        }
        
        if (record.getZp() != null) {
            sql.VALUES("zp", "#{zp,jdbcType=LONGVARBINARY}");
        }
        
        return sql.toString();
    }

    public String selectByExampleWithBLOBs(TZpGrwxzxxExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("xgh");
        } else {
            sql.SELECT("xgh");
        }
        sql.SELECT("wxz");
        sql.SELECT("zp");
        sql.FROM("T_ZP_GRWXZXX");
        applyWhere(sql, example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }
        
        return sql.toString();
    }

    public String selectByExample(TZpGrwxzxxExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("xgh");
        } else {
            sql.SELECT("xgh");
        }
        sql.SELECT("wxz");
        sql.FROM("T_ZP_GRWXZXX");
        applyWhere(sql, example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }
        
        return sql.toString();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        TZpGrwxzxx record = (TZpGrwxzxx) parameter.get("record");
        TZpGrwxzxxExample example = (TZpGrwxzxxExample) parameter.get("example");
        
        SQL sql = new SQL();
        sql.UPDATE("T_ZP_GRWXZXX");
        
        if (record.getXgh() != null) {
            sql.SET("xgh = #{record.xgh,jdbcType=VARCHAR}");
        }
        
        if (record.getWxz() != null) {
            sql.SET("wxz = #{record.wxz,jdbcType=VARCHAR}");
        }
        
        if (record.getZp() != null) {
            sql.SET("zp = #{record.zp,jdbcType=LONGVARBINARY}");
        }
        
        applyWhere(sql, example, true);
        return sql.toString();
    }

    public String updateByExampleWithBLOBs(Map<String, Object> parameter) {
        SQL sql = new SQL();
        sql.UPDATE("T_ZP_GRWXZXX");
        
        sql.SET("xgh = #{record.xgh,jdbcType=VARCHAR}");
        sql.SET("wxz = #{record.wxz,jdbcType=VARCHAR}");
        sql.SET("zp = #{record.zp,jdbcType=LONGVARBINARY}");
        
        TZpGrwxzxxExample example = (TZpGrwxzxxExample) parameter.get("example");
        applyWhere(sql, example, true);
        return sql.toString();
    }

    public String updateByExample(Map<String, Object> parameter) {
        SQL sql = new SQL();
        sql.UPDATE("T_ZP_GRWXZXX");
        
        sql.SET("xgh = #{record.xgh,jdbcType=VARCHAR}");
        sql.SET("wxz = #{record.wxz,jdbcType=VARCHAR}");
        
        TZpGrwxzxxExample example = (TZpGrwxzxxExample) parameter.get("example");
        applyWhere(sql, example, true);
        return sql.toString();
    }

    protected void applyWhere(SQL sql, TZpGrwxzxxExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            sql.WHERE(sb.toString());
        }
    }
}