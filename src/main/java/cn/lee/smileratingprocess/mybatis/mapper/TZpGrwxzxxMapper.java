package cn.lee.smileratingprocess.mybatis.mapper;

import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxx;
import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxxExample;
import java.util.List;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface TZpGrwxzxxMapper {
    @SelectProvider(type=TZpGrwxzxxSqlProvider.class, method="countByExample")
    long countByExample(TZpGrwxzxxExample example);

    @DeleteProvider(type=TZpGrwxzxxSqlProvider.class, method="deleteByExample")
    int deleteByExample(TZpGrwxzxxExample example);

    @Insert({
        "insert into T_ZP_GRWXZXX (xgh, wxz, ",
        "zp)",
        "values (#{xgh,jdbcType=VARCHAR}, #{wxz,jdbcType=VARCHAR}, ",
        "#{zp,jdbcType=LONGVARBINARY})"
    })
    int insert(TZpGrwxzxx record);

    @InsertProvider(type=TZpGrwxzxxSqlProvider.class, method="insertSelective")
    int insertSelective(TZpGrwxzxx record);

    @SelectProvider(type=TZpGrwxzxxSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="xgh", property="xgh", jdbcType=JdbcType.VARCHAR),
        @Result(column="wxz", property="wxz", jdbcType=JdbcType.VARCHAR),
        @Result(column="zp", property="zp", jdbcType=JdbcType.LONGVARBINARY)
    })
    List<TZpGrwxzxx> selectByExampleWithBLOBsWithRowbounds(TZpGrwxzxxExample example, RowBounds rowBounds);

    @SelectProvider(type=TZpGrwxzxxSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="xgh", property="xgh", jdbcType=JdbcType.VARCHAR),
        @Result(column="wxz", property="wxz", jdbcType=JdbcType.VARCHAR),
        @Result(column="zp", property="zp", jdbcType=JdbcType.LONGVARBINARY)
    })
    List<TZpGrwxzxx> selectByExampleWithBLOBs(TZpGrwxzxxExample example);

    @SelectProvider(type=TZpGrwxzxxSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="xgh", property="xgh", jdbcType=JdbcType.VARCHAR),
        @Result(column="wxz", property="wxz", jdbcType=JdbcType.VARCHAR)
    })
    List<TZpGrwxzxx> selectByExampleWithRowbounds(TZpGrwxzxxExample example, RowBounds rowBounds);

    @SelectProvider(type=TZpGrwxzxxSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="xgh", property="xgh", jdbcType=JdbcType.VARCHAR),
        @Result(column="wxz", property="wxz", jdbcType=JdbcType.VARCHAR)
    })
    List<TZpGrwxzxx> selectByExample(TZpGrwxzxxExample example);

    @UpdateProvider(type=TZpGrwxzxxSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") TZpGrwxzxx record, @Param("example") TZpGrwxzxxExample example);

    @UpdateProvider(type=TZpGrwxzxxSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") TZpGrwxzxx record, @Param("example") TZpGrwxzxxExample example);

    @UpdateProvider(type=TZpGrwxzxxSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") TZpGrwxzxx record, @Param("example") TZpGrwxzxxExample example);
}