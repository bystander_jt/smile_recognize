package cn.lee.smileratingprocess.mybatis.mapper;

import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxb;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxbExample.Criteria;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxbExample.Criterion;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxbExample;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.jdbc.SQL;

public class TYktYktzpxxbSqlProvider {

    public String countByExample(TYktYktzpxxbExample example) {
        SQL sql = new SQL();
        sql.SELECT("count(*)").FROM("T_YKT_YKTZPXXB");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    public String deleteByExample(TYktYktzpxxbExample example) {
        SQL sql = new SQL();
        sql.DELETE_FROM("T_YKT_YKTZPXXB");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    public String insertSelective(TYktYktzpxxb record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("T_YKT_YKTZPXXB");
        
        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }
        
        if (record.getRyh() != null) {
            sql.VALUES("ryh", "#{ryh,jdbcType=VARCHAR}");
        }
        
        if (record.getZt() != null) {
            sql.VALUES("zt", "#{zt,jdbcType=VARCHAR}");
        }
        
        if (record.getTstamp() != null) {
            sql.VALUES("tstamp", "#{tstamp,jdbcType=VARCHAR}");
        }
        
        if (record.getZp() != null) {
            sql.VALUES("zp", "#{zp,jdbcType=BLOB}");
        }
        
        return sql.toString();
    }

    public String selectByExampleWithBLOBs(TYktYktzpxxbExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("id");
        } else {
            sql.SELECT("id");
        }
        sql.SELECT("ryh");
        sql.SELECT("zt");
        sql.SELECT("tstamp");
        sql.SELECT("zp");
        sql.FROM("T_YKT_YKTZPXXB");
        applyWhere(sql, example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }
        
        return sql.toString();
    }

    public String selectByExample(TYktYktzpxxbExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("id");
        } else {
            sql.SELECT("id");
        }
        sql.SELECT("ryh");
        sql.SELECT("zt");
        sql.SELECT("tstamp");
        sql.FROM("T_YKT_YKTZPXXB");
        applyWhere(sql, example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }
        
        return sql.toString();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        TYktYktzpxxb record = (TYktYktzpxxb) parameter.get("record");
        TYktYktzpxxbExample example = (TYktYktzpxxbExample) parameter.get("example");
        
        SQL sql = new SQL();
        sql.UPDATE("T_YKT_YKTZPXXB");
        
        if (record.getId() != null) {
            sql.SET("id = #{record.id,jdbcType=VARCHAR}");
        }
        
        if (record.getRyh() != null) {
            sql.SET("ryh = #{record.ryh,jdbcType=VARCHAR}");
        }
        
        if (record.getZt() != null) {
            sql.SET("zt = #{record.zt,jdbcType=VARCHAR}");
        }
        
        if (record.getTstamp() != null) {
            sql.SET("tstamp = #{record.tstamp,jdbcType=VARCHAR}");
        }
        
        if (record.getZp() != null) {
            sql.SET("zp = #{record.zp,jdbcType=BLOB}");
        }
        
        applyWhere(sql, example, true);
        return sql.toString();
    }

    public String updateByExampleWithBLOBs(Map<String, Object> parameter) {
        SQL sql = new SQL();
        sql.UPDATE("T_YKT_YKTZPXXB");
        
        sql.SET("id = #{record.id,jdbcType=VARCHAR}");
        sql.SET("ryh = #{record.ryh,jdbcType=VARCHAR}");
        sql.SET("zt = #{record.zt,jdbcType=VARCHAR}");
        sql.SET("tstamp = #{record.tstamp,jdbcType=VARCHAR}");
        sql.SET("zp = #{record.zp,jdbcType=BLOB}");
        
        TYktYktzpxxbExample example = (TYktYktzpxxbExample) parameter.get("example");
        applyWhere(sql, example, true);
        return sql.toString();
    }

    public String updateByExample(Map<String, Object> parameter) {
        SQL sql = new SQL();
        sql.UPDATE("T_YKT_YKTZPXXB");
        
        sql.SET("id = #{record.id,jdbcType=VARCHAR}");
        sql.SET("ryh = #{record.ryh,jdbcType=VARCHAR}");
        sql.SET("zt = #{record.zt,jdbcType=VARCHAR}");
        sql.SET("tstamp = #{record.tstamp,jdbcType=VARCHAR}");
        
        TYktYktzpxxbExample example = (TYktYktzpxxbExample) parameter.get("example");
        applyWhere(sql, example, true);
        return sql.toString();
    }

    protected void applyWhere(SQL sql, TYktYktzpxxbExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            sql.WHERE(sb.toString());
        }
    }
}