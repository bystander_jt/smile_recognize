package cn.lee.smileratingprocess.mybatis.mapper;

import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxb;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxbExample;
import java.util.List;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface TYktYktzpxxbMapper {
    @SelectProvider(type=TYktYktzpxxbSqlProvider.class, method="countByExample")
    long countByExample(TYktYktzpxxbExample example);

    @DeleteProvider(type=TYktYktzpxxbSqlProvider.class, method="deleteByExample")
    int deleteByExample(TYktYktzpxxbExample example);

    @Insert({
        "insert into T_YKT_YKTZPXXB (id, ryh, ",
        "zt, tstamp, zp)",
        "values (#{id,jdbcType=VARCHAR}, #{ryh,jdbcType=VARCHAR}, ",
        "#{zt,jdbcType=VARCHAR}, #{tstamp,jdbcType=VARCHAR}, #{zp,jdbcType=BLOB})"
    })
    int insert(TYktYktzpxxb record);

    @InsertProvider(type=TYktYktzpxxbSqlProvider.class, method="insertSelective")
    int insertSelective(TYktYktzpxxb record);

    @SelectProvider(type=TYktYktzpxxbSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ryh", property="ryh", jdbcType=JdbcType.VARCHAR),
        @Result(column="zt", property="zt", jdbcType=JdbcType.VARCHAR),
        @Result(column="tstamp", property="tstamp", jdbcType=JdbcType.VARCHAR),
        @Result(column="zp", property="zp", jdbcType=JdbcType.BLOB)
    })
    List<TYktYktzpxxb> selectByExampleWithBLOBsWithRowbounds(TYktYktzpxxbExample example, RowBounds rowBounds);

    @SelectProvider(type=TYktYktzpxxbSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ryh", property="ryh", jdbcType=JdbcType.VARCHAR),
        @Result(column="zt", property="zt", jdbcType=JdbcType.VARCHAR),
        @Result(column="tstamp", property="tstamp", jdbcType=JdbcType.VARCHAR),
        @Result(column="zp", property="zp", jdbcType=JdbcType.BLOB)
    })
    List<TYktYktzpxxb> selectByExampleWithBLOBs(TYktYktzpxxbExample example);

    @SelectProvider(type=TYktYktzpxxbSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ryh", property="ryh", jdbcType=JdbcType.VARCHAR),
        @Result(column="zt", property="zt", jdbcType=JdbcType.VARCHAR),
        @Result(column="tstamp", property="tstamp", jdbcType=JdbcType.VARCHAR)
    })
    List<TYktYktzpxxb> selectByExampleWithRowbounds(TYktYktzpxxbExample example, RowBounds rowBounds);

    @SelectProvider(type=TYktYktzpxxbSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="ryh", property="ryh", jdbcType=JdbcType.VARCHAR),
        @Result(column="zt", property="zt", jdbcType=JdbcType.VARCHAR),
        @Result(column="tstamp", property="tstamp", jdbcType=JdbcType.VARCHAR)
    })
    List<TYktYktzpxxb> selectByExample(TYktYktzpxxbExample example);

    @UpdateProvider(type=TYktYktzpxxbSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") TYktYktzpxxb record, @Param("example") TYktYktzpxxbExample example);

    @UpdateProvider(type=TYktYktzpxxbSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") TYktYktzpxxb record, @Param("example") TYktYktzpxxbExample example);

    @UpdateProvider(type=TYktYktzpxxbSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") TYktYktzpxxb record, @Param("example") TYktYktzpxxbExample example);
}