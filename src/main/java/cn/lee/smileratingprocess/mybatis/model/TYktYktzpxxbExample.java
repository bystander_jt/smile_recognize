package cn.lee.smileratingprocess.mybatis.model;

import java.util.ArrayList;
import java.util.List;

public class TYktYktzpxxbExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TYktYktzpxxbExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRyhIsNull() {
            addCriterion("ryh is null");
            return (Criteria) this;
        }

        public Criteria andRyhIsNotNull() {
            addCriterion("ryh is not null");
            return (Criteria) this;
        }

        public Criteria andRyhEqualTo(String value) {
            addCriterion("ryh =", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhNotEqualTo(String value) {
            addCriterion("ryh <>", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhGreaterThan(String value) {
            addCriterion("ryh >", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhGreaterThanOrEqualTo(String value) {
            addCriterion("ryh >=", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhLessThan(String value) {
            addCriterion("ryh <", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhLessThanOrEqualTo(String value) {
            addCriterion("ryh <=", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhLike(String value) {
            addCriterion("ryh like", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhNotLike(String value) {
            addCriterion("ryh not like", value, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhIn(List<String> values) {
            addCriterion("ryh in", values, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhNotIn(List<String> values) {
            addCriterion("ryh not in", values, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhBetween(String value1, String value2) {
            addCriterion("ryh between", value1, value2, "ryh");
            return (Criteria) this;
        }

        public Criteria andRyhNotBetween(String value1, String value2) {
            addCriterion("ryh not between", value1, value2, "ryh");
            return (Criteria) this;
        }

        public Criteria andZtIsNull() {
            addCriterion("zt is null");
            return (Criteria) this;
        }

        public Criteria andZtIsNotNull() {
            addCriterion("zt is not null");
            return (Criteria) this;
        }

        public Criteria andZtEqualTo(String value) {
            addCriterion("zt =", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtNotEqualTo(String value) {
            addCriterion("zt <>", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtGreaterThan(String value) {
            addCriterion("zt >", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtGreaterThanOrEqualTo(String value) {
            addCriterion("zt >=", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtLessThan(String value) {
            addCriterion("zt <", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtLessThanOrEqualTo(String value) {
            addCriterion("zt <=", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtLike(String value) {
            addCriterion("zt like", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtNotLike(String value) {
            addCriterion("zt not like", value, "zt");
            return (Criteria) this;
        }

        public Criteria andZtIn(List<String> values) {
            addCriterion("zt in", values, "zt");
            return (Criteria) this;
        }

        public Criteria andZtNotIn(List<String> values) {
            addCriterion("zt not in", values, "zt");
            return (Criteria) this;
        }

        public Criteria andZtBetween(String value1, String value2) {
            addCriterion("zt between", value1, value2, "zt");
            return (Criteria) this;
        }

        public Criteria andZtNotBetween(String value1, String value2) {
            addCriterion("zt not between", value1, value2, "zt");
            return (Criteria) this;
        }

        public Criteria andTstampIsNull() {
            addCriterion("tstamp is null");
            return (Criteria) this;
        }

        public Criteria andTstampIsNotNull() {
            addCriterion("tstamp is not null");
            return (Criteria) this;
        }

        public Criteria andTstampEqualTo(String value) {
            addCriterion("tstamp =", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampNotEqualTo(String value) {
            addCriterion("tstamp <>", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampGreaterThan(String value) {
            addCriterion("tstamp >", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampGreaterThanOrEqualTo(String value) {
            addCriterion("tstamp >=", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampLessThan(String value) {
            addCriterion("tstamp <", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampLessThanOrEqualTo(String value) {
            addCriterion("tstamp <=", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampLike(String value) {
            addCriterion("tstamp like", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampNotLike(String value) {
            addCriterion("tstamp not like", value, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampIn(List<String> values) {
            addCriterion("tstamp in", values, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampNotIn(List<String> values) {
            addCriterion("tstamp not in", values, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampBetween(String value1, String value2) {
            addCriterion("tstamp between", value1, value2, "tstamp");
            return (Criteria) this;
        }

        public Criteria andTstampNotBetween(String value1, String value2) {
            addCriterion("tstamp not between", value1, value2, "tstamp");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}