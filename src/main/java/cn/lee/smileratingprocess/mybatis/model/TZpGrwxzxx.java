package cn.lee.smileratingprocess.mybatis.model;

import java.io.Serializable;
import java.util.Arrays;

public class TZpGrwxzxx implements Serializable {
    private String xgh;

    private String wxz;

    private byte[] zp;

    private static final long serialVersionUID = 1L;

    public String getXgh() {
        return xgh;
    }

    public void setXgh(String xgh) {
        this.xgh = xgh == null ? null : xgh.trim();
    }

    public String getWxz() {
        return wxz;
    }

    public void setWxz(String wxz) {
        this.wxz = wxz == null ? null : wxz.trim();
    }

    public byte[] getZp() {
        return zp;
    }

    public void setZp(byte[] zp) {
        this.zp = zp;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TZpGrwxzxx other = (TZpGrwxzxx) that;
        return (this.getXgh() == null ? other.getXgh() == null : this.getXgh().equals(other.getXgh()))
            && (this.getWxz() == null ? other.getWxz() == null : this.getWxz().equals(other.getWxz()))
            && (Arrays.equals(this.getZp(), other.getZp()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getXgh() == null) ? 0 : getXgh().hashCode());
        result = prime * result + ((getWxz() == null) ? 0 : getWxz().hashCode());
        result = prime * result + (Arrays.hashCode(getZp()));
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", xgh=").append(xgh);
        sb.append(", wxz=").append(wxz);
        sb.append(", zp=").append(zp);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}