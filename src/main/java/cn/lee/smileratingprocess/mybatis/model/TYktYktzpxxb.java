package cn.lee.smileratingprocess.mybatis.model;

import java.io.Serializable;
import java.util.Arrays;

public class TYktYktzpxxb implements Serializable {
    private String id;

    private String ryh;

    private String zt;

    private String tstamp;

    private byte[] zp;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getRyh() {
        return ryh;
    }

    public void setRyh(String ryh) {
        this.ryh = ryh == null ? null : ryh.trim();
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt == null ? null : zt.trim();
    }

    public String getTstamp() {
        return tstamp;
    }

    public void setTstamp(String tstamp) {
        this.tstamp = tstamp == null ? null : tstamp.trim();
    }

    public byte[] getZp() {
        return zp;
    }

    public void setZp(byte[] zp) {
        this.zp = zp;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TYktYktzpxxb other = (TYktYktzpxxb) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getRyh() == null ? other.getRyh() == null : this.getRyh().equals(other.getRyh()))
            && (this.getZt() == null ? other.getZt() == null : this.getZt().equals(other.getZt()))
            && (this.getTstamp() == null ? other.getTstamp() == null : this.getTstamp().equals(other.getTstamp()))
            && (Arrays.equals(this.getZp(), other.getZp()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getRyh() == null) ? 0 : getRyh().hashCode());
        result = prime * result + ((getZt() == null) ? 0 : getZt().hashCode());
        result = prime * result + ((getTstamp() == null) ? 0 : getTstamp().hashCode());
        result = prime * result + (Arrays.hashCode(getZp()));
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", ryh=").append(ryh);
        sb.append(", zt=").append(zt);
        sb.append(", tstamp=").append(tstamp);
        sb.append(", zp=").append(zp);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}