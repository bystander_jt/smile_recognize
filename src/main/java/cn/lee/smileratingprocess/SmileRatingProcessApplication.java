package cn.lee.smileratingprocess;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties
@MapperScan(basePackages = {"cn.lee.smileratingprocess.mybatis"})
public class SmileRatingProcessApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmileRatingProcessApplication.class, args);
    }

}
