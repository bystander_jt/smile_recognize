package cn.lee.smileratingprocess.constants;


import org.apache.http.HttpStatus;

import java.io.Serializable;

public class  Result<T> implements Serializable {



    private int code;

    private boolean success;

    private String message;

    private T data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public Result(int code, boolean success, String message, T data) {
        this.code = code;
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public Result(int code, boolean success, String message) {
        this.code = code;
        this.success = success;
        this.message = message;
    }

    public static Result OK(){
        return new Result(HttpStatus.SC_OK, true, "success");
    }

    public static Result OK(String message){
        return new Result(HttpStatus.SC_OK, true, message);
    }


    public static Result fail(String message){
        return new Result(HttpStatus.SC_OK, false, message);
    }


}
