package cn.lee.smileratingprocess.controller;


import cn.lee.smileratingprocess.constants.Result;
import cn.lee.smileratingprocess.service.dao.UserDAO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/")
public class UploadController {

    @Autowired
    private UserDAO userDAO;

    private static final String PATH = new StringBuilder(System.getProperty("user.dir"))
            .append(File.separator)
            .append("tmp").toString();

    private static final List<String> ALLOW_UPLOAD_TYPE = Arrays.asList("jpeg", "png", "jpg");

    @PostMapping("upload")
    public Result upload(@RequestParam MultipartFile file) {
        if (file.isEmpty()) return Result.fail("file can not be empty");

        if (!preCheck(file.getOriginalFilename())) return Result.fail("not allow upload file type");

        try {
            File tmpFile = createTmpFile(file);
            userDAO.insertUser(tmpFile);
        } catch (IOException e) {
            log.error("upload has exceptions message : {}", e.getMessage(), e);
        }
        return Result.OK();
    }

    private boolean preCheck(String originalName) {
        if (StringUtils.isBlank(originalName)) return false;

        String[] split = originalName.split("\\.");
        String suffix = split[split.length - 1];

        return ALLOW_UPLOAD_TYPE.contains(suffix);

    }

    private File createTmpFile(MultipartFile file) throws IOException {
        String uid = UUID.randomUUID().toString();

        String filename = file.getOriginalFilename();

        String suffix = Optional.ofNullable(filename).filter(d -> d.contains(".")).map(d -> d.substring(d.lastIndexOf('.') + 1)).orElse(null);

        StringBuilder tmpPath = new StringBuilder(PATH).append(File.separator).append(uid);
        Optional.ofNullable(suffix).filter(StringUtils::isNotBlank).ifPresent(d -> tmpPath.append('.').append(d));

        File tmpFile = new File(tmpPath.toString());
        tmpFile.getParentFile().mkdirs();
        file.transferTo(tmpFile);
        return tmpFile;
    }

}
