package cn.lee.smileratingprocess.configuration;

import com.aliyun.facebody20191230.Client;
import com.aliyun.teaopenapi.models.Config;
import com.aliyuncs.http.HttpClientConfig;
import com.aliyuncs.http.HttpClientType;
import com.aliyuncs.profile.DefaultProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AliCloudConfiguration {

    private static final Logger log = LoggerFactory.getLogger(AliCloudConfiguration.class);

    @Value("${aliyun.id}")
    private String accessKey;
    @Value("${aliyun.secret}")
    private String secret;
    @Value("${aliyun.region}")
    private String regionId;

    @Bean
    public DefaultProfile aliCloudProfile() {
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKey, secret);

        HttpClientConfig config = new HttpClientConfig();
        config.setClientType(HttpClientType.ApacheHttpClient);
        config.setConnectionTimeoutMillis(1000 * 10);

        profile.setHttpClientConfig(config);
        return profile;
    }

    @Bean
    public Client aliClient() {
        try {
            Config config = new Config();
            config.setAccessKeyId(accessKey)
                    .setAccessKeySecret(secret)
                    .setRegionId(regionId)
                    .setType("access_key")
                    // 人脸人体检测专属域名
                    .setEndpoint("facebody.cn-shanghai.aliyuncs.com");

            return new Client(config);
        } catch (Exception e) {
            log.error("aliClient initialize failure, message {}", e.getMessage());
            return null;
        }
    }


}
