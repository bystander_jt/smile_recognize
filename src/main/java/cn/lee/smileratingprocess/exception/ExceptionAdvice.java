package cn.lee.smileratingprocess.exception;

import cn.lee.smileratingprocess.constants.Result;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
@ControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(RuntimeException.class)
    public Result runtimeException(RuntimeException e){
        return Result.fail(e.getMessage());
    }

}
