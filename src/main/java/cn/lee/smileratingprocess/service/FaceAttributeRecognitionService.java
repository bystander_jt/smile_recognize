package cn.lee.smileratingprocess.service;


import com.aliyun.facebody20191230.Client;
import com.aliyun.facebody20191230.models.RecognizeFaceAdvanceRequest;
import com.aliyun.facebody20191230.models.RecognizeFaceResponse;
import com.aliyun.facebody20191230.models.RecognizeFaceResponseBody;
import com.aliyun.teautil.models.RuntimeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class FaceAttributeRecognitionService {

    private static final Logger log = LoggerFactory.getLogger(FaceAttributeRecognitionService.class);

    private static AtomicLong LAST_REQUEST = new AtomicLong(0);

    private static final RuntimeOptions OPTIONS;

    static {
        OPTIONS = new RuntimeOptions();
        OPTIONS.readTimeout = 1000;
        OPTIONS.autoretry = true;
        OPTIONS.maxAttempts = 3;
        OPTIONS.connectTimeout = 1000;
    }

    @Autowired
    private Client aliClient;

    /**
     * 人脸属性识别
     *
     * @param image 图片 base64
     * @return
     */
    public Number recognize(String uid, byte[] image) {
        try {
            long now = System.currentTimeMillis();
            if (image.length <= 0) {
                log.warn("user [{}] image size too small ( size < 0)", uid);
            }
            RecognizeFaceAdvanceRequest req = new RecognizeFaceAdvanceRequest();
            req.setImageURLObject(new ByteArrayInputStream(image));

            if (LAST_REQUEST.longValue() + 500 > now) {
                Thread.sleep(now - (LAST_REQUEST.longValue() + 500));
            }

            RecognizeFaceResponse response = aliClient.recognizeFaceAdvance(req, OPTIONS);
            RecognizeFaceResponseBody.RecognizeFaceResponseBodyData data = response.getBody().getData();

            LAST_REQUEST.set(System.currentTimeMillis());
            return data.getBeautyList().get(0);
        } catch (Exception e) {
            log.error("student id [{}] request failure, message : {}", uid, e.getMessage());
            return null;
        }
    }

}
