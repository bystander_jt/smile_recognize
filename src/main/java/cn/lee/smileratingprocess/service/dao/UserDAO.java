package cn.lee.smileratingprocess.service.dao;

import cn.hutool.core.collection.CollectionUtil;
import cn.lee.smileratingprocess.mybatis.mapper.TYktYktzpxxbMapper;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxb;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxbExample;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.ibatis.type.JdbcType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class UserDAO {

    @Autowired
    private TYktYktzpxxbMapper userMapper;

    public Long count() {
        return Optional.of(userMapper.countByExample(new TYktYktzpxxbExample())).orElse(0L);
    }

    public void insertUser(File file) {
        BufferedInputStream inputStream = null;
        try {
            TYktYktzpxxb user = new TYktYktzpxxb();
            user.setId(UUID.randomUUID().toString());
            user.setRyh(String.valueOf(RandomUtils.nextLong(1000L, 9999999L)));

            inputStream = new BufferedInputStream(new FileInputStream(file));
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);


            user.setZp(bytes);
            userMapper.insertSelective(user);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public TYktYktzpxxb findBinaryFile(String uid){
        TYktYktzpxxbExample example = new TYktYktzpxxbExample();
        example.createCriteria().andIdEqualTo(uid);
        return userMapper.selectByExampleWithBLOBs(example).stream().findFirst().orElse(null);
    }


}
