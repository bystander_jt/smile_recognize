package cn.lee.smileratingprocess.service.dao;

import cn.lee.smileratingprocess.mybatis.ext.RecognizedExtMapper;
import cn.lee.smileratingprocess.mybatis.mapper.TZpGrwxzxxMapper;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxb;
import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxx;
import cn.lee.smileratingprocess.mybatis.model.TZpGrwxzxxExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecognizeDAO {

    private static final Logger log = LoggerFactory.getLogger(RecognizeDAO.class);

    @Autowired
    private TZpGrwxzxxMapper mapper;
    @Autowired
    private RecognizedExtMapper recognizedExtMapper;

    public boolean exist(String uid) {
        TZpGrwxzxxExample example = new TZpGrwxzxxExample();
        example.createCriteria().andXghEqualTo(uid);
        return mapper.countByExample(example) > 0;
    }

    public void insert(TYktYktzpxxb user, Long beauty) {
        TZpGrwxzxx record = new TZpGrwxzxx();
        record.setXgh(user.getRyh());
        record.setZp(user.getZp());
        record.setWxz(beauty.toString());
        mapper.insert(record);
    }

}
