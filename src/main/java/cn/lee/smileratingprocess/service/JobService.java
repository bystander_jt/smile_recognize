package cn.lee.smileratingprocess.service;

import cn.lee.smileratingprocess.mybatis.ext.RecognizedExtMapper;
import cn.lee.smileratingprocess.mybatis.model.TYktYktzpxxb;
import cn.lee.smileratingprocess.service.dao.RecognizeDAO;
import cn.lee.smileratingprocess.service.dao.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class JobService {

    private static final Logger log = LoggerFactory.getLogger(JobService.class);

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private RecognizeDAO recognizeDAO;

    @Autowired
    private FaceAttributeRecognitionService recognitionService;
    @Autowired
    private RecognizedExtMapper recognizedExtMapper;

    public void startJob() {
        log.info("start recognize job");
        Long totalUser = userDAO.count();
        List<String> unRecognizedIds = recognizedExtMapper.findUnRecognizedIds();
        log.info("total users : {}, unrecognized total : {}", totalUser, unRecognizedIds);

        startRecognize(unRecognizedIds);

        log.info("recognize job finished !");

    }

    public void startRecognize(List<String> userIds) {
        if (CollectionUtils.isEmpty(userIds)) return;

        for (String userId : userIds) {

            boolean exist = recognizeDAO.exist(userId);
            if (exist) {
                log.debug("user [{}] has benn recognized . skip", userId);
                continue;
            }

            TYktYktzpxxb user = userDAO.findBinaryFile(userId);
            if (user == null) {
                log.warn("cannot find user : {}", userId);
                continue;
            }

            Long recognizeScore = (Long) recognitionService.recognize(userId, user.getZp());
            if (recognizeScore == null) {
                log.warn("user [{}] recognize failure !", userId);
                continue;
            }

            recognizeDAO.insert(user, recognizeScore);


        }

    }

}
